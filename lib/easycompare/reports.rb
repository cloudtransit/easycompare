module Reports

  require 'fileutils'

  # Module for Tex reports.
  #
  module Tex

    def self.start_report(title)
      [
          "\\documentclass{article}",
          "\\usepackage{rotating}",
          "\\usepackage[table]{xcolor}",
          "\\title{#{title}}",
          "\\begin{document}",
          "\\maketitle",
          "\\listoftables",
          "\\listoffigures",
      ]
    end

    def self.end_report
      [
        "\\end{document}"
      ]
    end

    def self.provider_report(data, title, dir)

      FileUtils.mkdir_p ["#{dir}/R", "#{dir}/eps"]
      r = RinRuby.new(:echo=>false)

      provider_data = data.values.group_by { |entry| entry[:iaas] }
      providers = provider_data.keys.combination(2)

      similarities = providers.map do |p1, p2|
        Visualizer::Tex::similarity_matrix(provider_data[p1], provider_data[p2], "Similarity of #{p1} and #{p2} VM types")
      end * "\n"


      report = start_report("Provider report: #{title}")

      figures = provider_data.map do |provider, data|

        # Processing
        File.write(
            "#{dir}/R/#{provider}_processing.R",
            Visualizer::R::boxplot(data, :processing, 'Computation performance (smaller is better)', 'Taychon, Time [s]', :provider, true)
        )

        r.eval """
          setEPS()
          postscript('#{dir}/eps/#{provider}_processing.eps')
          source('#{dir}/R/#{provider}_processing.R')
          dev.off()
        """


        # Memory
        File.write(
            "#{dir}/R/#{provider}_memory.R",
            Visualizer::R::boxplot(data, :memory, 'Memory transfer (higher is better)', 'Stream, Triad [MB/s]', :provider, true)
        )

        r.eval """
          setEPS()
          postscript('#{dir}/eps/#{provider}_memory.eps')
          source('#{dir}/R/#{provider}_memory.R')
          dev.off()
        """

        # Network
        File.write(
            "#{dir}/R/#{provider}_network.R",
            Visualizer::R::boxplot(data, :net, 'Network transfer (higher is better)', 'iPerf [MB/s]', :provider, true)
        )

        r.eval """
          setEPS()
          postscript('#{dir}/eps/#{provider}_network.eps')
          source('#{dir}/R/#{provider}_network.R')
          dev.off()
        """

        # Disk I/O Read
        File.write(
            "#{dir}/R/#{provider}_read_io.R",
            Visualizer::R::boxplot(data, :reads, 'Disk I/O Read (higher is better)', 'IOZone, Read [MB/s]', :provider, true)
        )

        r.eval """
          setEPS()
          postscript('#{dir}/eps/#{provider}_read_io.eps')
          source('#{dir}/R/#{provider}_read_io.R')
          dev.off()
        """

        # Disk I/O Write
        File.write(
            "#{dir}/R/#{provider}_write_io.R",
            Visualizer::R::boxplot(data, :writes, 'Disk I/O Write (higher is better)', 'IOZone, Write [MB/s]', :provider, true)
        )

        r.eval """
          setEPS()
          postscript('#{dir}/eps/#{provider}_write_io.eps')
          source('#{dir}/R/#{provider}_write_io.R')
          dev.off()
        """

        [provider, {
            'processing' => "eps/#{provider}_processing.eps",
            'memory' => "eps/#{provider}_memory.eps",
            'network' => "eps/#{provider}_network.eps",
            'read' => "eps/#{provider}_read_io.eps",
            'write' => "eps/#{provider}_write_io.eps"
         }]
      end.to_h




      performance = figures.map do |provider, figures|
        "\\subsection{Performance data of provider #{provider}}" +
        figures.map do |figure, eps|
          """
          \\begin{figure}[htbf]
          \\centering
          \\includegraphics[width=0.95\\textwidth]{#{eps}}
          \\caption{#{figure} for #{provider}}
          \\label{fig:#{figure}_#{provider}}
          \\end{figure}
          """
        end * "\n"
      end

      report << [
          "\\section{Similarity Analysis}",
          "#{similarities}",
          "\\section{Performance Analysis}"
      ]
      report << performance
      report << [
          "\\section{Performance Summary}",
          Visualizer::Tex::instances_table(data, 'Median Performance Data')
      ]
      report << end_report
      report * "\n"
    end

    def self.similarity_report(data, title)
      report = start_report("Similarity report: #{title}")
      report << [
          "Hello World"
      ]
      report << end_report
      report * "\n"
    end

  end

  # Module for HTML reports
  #
  module Html

    require 'haml'
    require 'nokogiri'
    require 'awesome_print'

    def self.stylesheet
      [
        ":sass",
        "  @import url(https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css)",
        "  @import url(http://fonts.googleapis.com/css?family=Open+Sans)",

        # LINE STYLING PARAMETERS
        "  $thinline: 1px",
        "  $fatline: 2px",

        # COLOR STYLING PARAMETERS
        "  $dark: rgb(34, 34, 34)",
        "  $med: lightgrey",
        "  $header-background: linear-gradient(to bottom, rgba(64,150,238,1) 0%,rgba(96,171,248,1) 50%, rgba(122,188,255,1) 100%)",
        "  $header-font: white",
        "  $footer-background: rgba(0, 0, 0, 0.7)",
        "  $footer-color: white",
        "  $body-background: #F2F2F2",
        "  $headline-background: rgba(0,0,0,0.7)",
        "  $headline-color: rgba(255, 255, 255, 0.7)",
        "  $dark-transparent: rgba(0, 0, 0, 0.2)",
        "  $light-transparent: rgba(255, 255, 255, 0.8)",

        # MARGIN STYLING PARAMETERS
        "  $small: 5px",
        "  $medium: 10px",
        "  $big: 20px",
        "  $xxl: 40px",

        # FONT STYLING PARAMETERS
        "  $smaller_font: 0.95em",
        "  $primary_font: 'Open Sans', sans-serif",

        # SHADOW STYLING PARAMETERS
        "  $shadow_width: 5px",
        "  $shadow: $shadow_width $shadow_width $shadow_width $med",

        # GENERAL TYPOGRAPHIC STYLING
        "  body",
        "    font-family: $primary_font",
        "    background: $body-background",

        # HEADER STYLING
        "  .navbar",
        "    margin-bottom: 0px !important",
        "    border-radius: 0px !important",

        "  .page-header",
        "    background: $header-background",
        "    color: $header-font",
        "    margin-top: 0px !important",
        "    margin-bottom: 0px !important",
        "    border-bottom: 0px !important",
        "    padding: $xxl",
        "    h1",
        "      font-weight: bold",
        "      margin-bottom: $small",
        "    h2",
        "      font-weight: light",
        "      margin-top: $small",
        "      margin-bottom: $xxl",
        "      font-style: italic",
        "      color: $light-transparent",
        "    .meta",
        "      margin: $big",
        "      padding: $big",
        "      background: $dark-transparent",
        "      color: $light-transparent",
        "      border-radius: $small",
        "      ul",
        "        padding: 0px",
        "        li",
        "          display: block",
        "          border-top: $thinline solid $light-transparent",
        "          padding: $small 0px $small 0px",

        # FOOTER STYLE
        "  .footer",
        "    margin-top: $xxl",
        "    padding: $xxl",
        "    background: $footer-background",
        "    color: $footer-color",
        "    text-align: center",

        # MEASURED DATA TABLE STYLING
        "  .measured_data",
        "    margin: $medium",
        "    padding: $big",
        "    box-shadow: $shadow",
        #"    border-radius: $medium",
        #"    border: $thinline dashed $dark",
        "    table",
        "      margin: $big",
        "      border-collapse: collapse",
        "      tr",
        "        border-bottom: $thinline solid $med",
        "      tr:first-child",
        "        border-bottom: $fatline solid $dark",
        "      tr:last-child",
        "        border-bottom: 0px solid transparent",
        "      th, td",
        "        padding: $small $medium $small $medium",
        "        text-align: right",
        "        font-size: $smaller_font",
        "        vertical-align: top",
        "        small",
        "          color: gray",
        "          font-style: italic",
        "          font-weight: normal",
        "      tr",
        "        td:first-child, th:first-child",
        "          text-align: left",
        "          width: auto",

        # COMPARISON PLOT STYLING

        "  .scatterplot, .boxplot, .measured_data, .similarity",
        "    margin: $medium",
        "    padding: $medium",
        "    box-shadow: $shadow",

#        "    border-radius: $medium",
#        "    border: $thinline dashed $dark",

        "  .page-header ~ .container h2",
        "    margin-top: $xxl",
        "    clear: both",
        "    padding: $big",
        "    margin: 0px",
        "    margin-top: $xxl",
        "    background-color: $headline-background",
        "    color: $headline-color",
        "    box-shadow: $shadow",


        # SHADOW STYLING
        "  .comparison, .variances, .summary",
        "    background-color: white",
        "    padding: $big",
        "    box-shadow: $shadow",
        "    border: $thinline solid $headline-background",

        # SIMILARITY MATRIX STYLING
        "  .similarity",
        "    table",
        "      margin: $big",
        "      margin-left: auto",
        "      margin-right: auto",
        "      caption",
        "        caption-side: bottom",
        "        text-align: center",
        "      border-collapse: collapse",
        "      th, td",
        "        padding: 5px",
        "      th",
        "        text-align: center",
        "        span.vertical",
        "          -webkit-writing-mode: vertical-lr",
        "          transform: rotate(180deg)",
        "          white-space: nowrap",
        "      td",
        "        text-align: center",
        "        vertical-align: center",
        "      td:first-child",
        "        text-align: right",
        "        white-space: nowwrap",
        "      tr",
        "        td",
        "          font-size: 0.75em",
        "        td:first-child",
        "          font-size: 0.9em",
        "          font-weight: bold",
        "      tr:first-child",
        "        th",
        "          vertical-align: bottom",
        "        th:first-child",
        "          vertical-align: middle",
        "          text-align: center",
        "          transform: rotate(45deg)",

      ]
    end

    def self.head(title)
      [
        "%html",
        "  %head",
        "    %title= '#{title}'",

        "    %script{:src => 'https://code.jquery.com/jquery-2.1.4.min.js'}",
        "    %script{:src => 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js'}",
        "    %script{:src => 'http://code.highcharts.com/highcharts.js'}",
        "    %script{:src => 'http://code.highcharts.com/highcharts-3d.js'}",
        "    %script{:src => 'http://code.highcharts.com/highcharts-more.js'}",
        "    %script{:src => 'http://code.highcharts.com/modules/exporting.js'}",
      ]
    end

    def self.header(title, subtitle, data)
      meta = Analyzer::meta(data)
      datacenters = "<ul>#{meta[:datacenters].map { |provider, centers| "<li>#{provider}: #{centers * ', ' }</li>" } * ''}</ul>"
      vmtypes = "<ul>#{meta[:vmtypes].map { |provider, types | "<li>#{provider}: #{types * ', ' }</li>"} * ''}</ul>"
      date = Date.today.strftime("%e. %B %Y")

      [
        "    .page-header",
        "      .container",
        "        %h1 #{title}",
        "        %h2 #{subtitle}",
        "        .meta",
        "          .row",
        "            .col-md-3",
        "              :plain",
        "                Datacenters:<ul>",
        "                #{datacenters}",
        "                </ul>",
        "            .col-md-6",
        "              :plain",
        "                Virtual Machine Types: #{vmtypes}",
        "            .col-md-3",
        "              :plain",
        "                Average Sample sizes per VM Type:<ul>",
        "                <li>Processing: n = #{meta[:samplesizes][:processing] }</li>",
        "                <li>Network:  n = #{meta[:samplesizes][:net] }</li>",
        "                <li>Disk I/O (read):  n = #{meta[:samplesizes][:reads]}</li>",
        "                <li>Disk I/O (write): n = #{meta[:samplesizes][:writes]}</li>",
        "                <li>Memory transfer: n = #{meta[:samplesizes][:memory]}</li>",
        "                <li>Timeframe of data collection:<br>",
        "                #{meta[:start].strftime("%e. %b %Y")} - #{meta[:end].strftime("%e. %b %Y")}",
        "                </li>",
        "                </ul>",
        "            .col-md-12",
        "              :plain",
        "                 Generated on #{date}.",
      ]
    end

    def self.footer
      [
        "    .footer",
        "      .container",
        "        .row",
        "          .col-md-4",
        "          .col-md-4",
        "            &copy; nkode.io 2015",
        "          .col-md-4",
      ]
    end

    def self.provider_report(data, title)

      providers = data.values.map { |machine| machine[:iaas] }.uniq

      doc = stylesheet + head(title) + [
        "  %body",
      ]
      doc << header(title, "of providers: #{ providers * ', ' }", data)
      doc <<
      [
        "    %nav.navbar.navbar-inverse",
        "      .container",
        "        %ul.nav.navbar-nav",
        "          %li",
        "            %a{:href => '#processing'} Processing",
        "          %li",
        "            %a{:href => '#memory'} Memory",
        "          %li",
        "            %a{:href => '#network'} Network",
        "          %li",
        "            %a{:href => '#disk_read'} Disk Read I/O",
        "          %li",
        "            %a{:href => '#disk_write'} Disk Write I/O",
        "          %li",
        "            %a{:href => '#summary'} Summary",

        "    .container",

        "      %h2#processing Processing Performance",
        "      .variances",
        "        .row",
        "          .col-md-12",
        "            :plain",
        "              #{Visualizer::HTML::boxplot(data, :processing, 'Processor performance (smaller is better)', 'Taychon, Time [s]', :provider, true, type: 'logarithmic')}",

        "      %h2#memory Memory Transfer Performance",
        "      .variances",
        "        .row",
        "          .col-md-12",
        "            :plain",
        "              #{Visualizer::HTML::boxplot(data, :memory, 'Memory transfer (higher is better)', 'Stream, Triad [MB/s]', :provider, true)}",

        "      %h2#disk_read Disk Read I/O Performance",
        "      .variances",
        "        .row",
        "          .col-md-12",
        "            :plain",
        "              #{Visualizer::HTML::boxplot(data, :reads, 'Disk Read I/O Performance (higher is better)', 'IOZone, Read [MB/s]', :provider, true)}",

        "      %h2#disk_write Disk Write I/O Performance",
        "      .variances",
        "        .row",
        "          .col-md-12",
        "            :plain",
        "              #{Visualizer::HTML::boxplot(data, :writes, 'Disk Write I/O Performance (higher is better)', 'IOZone, Write [MB/s]', :provider, true)}",

        "      %h2#network Network Transfer Performance",
        "      .variances",
        "        .row",
        "          .col-md-12",
        "            :plain",
        "              #{Visualizer::HTML::boxplot(data, :net, 'Network transfer (higher is better)', 'iperf [MB/s]', :provider, true)}",

        "      %h2#summary Summary of Virtual Machine Types",
        "      .summary",
        "        .row",
        "          .col-md-12",
        "            :plain",
        "              #{Visualizer::HTML::instances_table(data, title)}",
      ] + footer

      haml = Haml::Engine.new(doc.join("\n"))

      dom = Nokogiri::HTML(haml.render)
      dom.to_html
    end

    def self.similarity_report(data, title)

      series = data.values.group_by { |machine_data| machine_data[:iaas] }

      combinations = series.keys.combination(2).to_a

      similarity_matrices = combinations.map do |p1, p2|
        Visualizer::HTML::similarity_matrix(series[p1], series[p2], "Similarity of '#{p1}' and '#{p2}' virtual machine types")
      end

      doc = stylesheet + head(title) + [
        "  %body",
      ]
      doc << header(title, "of providers: #{ series.keys * ', ' }", data)
      doc << [
        "    %nav.navbar.navbar-inverse",
        "      .container",
        "        %ul.nav.navbar-nav",
        "          %li",
        "            %a{:href => '#comparison'} Comparison Plots",
        "          %li",
        "            %a{:href => '#similarity'} Similarity Matrices",

        "    .container",
        "      %h2#comparison Comparison Plots",
        "      .comparison",
        "        .row",
        "          .col-md-12",
        "            :plain",
        "             #{Visualizer::HTML::plot_scatter3d(
                          data,
                          "Processing/Memory Space",
                          "Memory size [MB]",
                          "Threads [n]",
                          "Memory transfer [MB/s]",
                          :xsel => :memsize,
                          :ysel => :threads,
                          :zsel => :memory
                      )}",
        "          .col-md-12",
        "            :plain",
        "             #{Visualizer::HTML::plot_scatter3d(
                          data,
                          "I/O Performance Space",
                          "Read I/O [MB/s]",
                          "Network [MB/s]",
                          "Write I/O [MB/s]",
                          :xsel => :reads, :ysel => :net, :zsel => :writes
                      )}",
        # "          .col-md-6",
        # "            :plain",
        # "              #{Visualizer::HTML::scatterplot(data, "Processing vs. Memory", "Processing (smaller is better)", "Memory (higher is better)", :processing, :memory)}",
        # "          .col-md-6",
        # "            :plain",
        # "              #{Visualizer::HTML::scatterplot(data, "Processing vs. Network", "Processing (smaller is better)", "Network I/O (higher is better)", :processing, :net)}",
        # "          .col-md-6",
        # "            :plain",
        # "              #{Visualizer::HTML::scatterplot(data, "Processing vs. Read I/O", "Processing (smaller is better)", "Disk Read I/O (higher is better)", :processing, :reads)}",
        # "          .col-md-6",
        # "            :plain",
        # "              #{Visualizer::HTML::scatterplot(data, "Processing vs. Write I/O", "Processing (smaller is better)", "Disk Write I/O (higher is better)", :processing, :writes)}",

        "    .container",
        "      %h2#similarity Similarity Matrices",
        "      .comparison",
        "        :plain",
        "          #{similarity_matrices * "\n" }"
      ] + footer

      haml = Haml::Engine.new(doc.join("\n"))
      dom = Nokogiri::HTML(haml.render)
      dom.to_html
    end

  end

end