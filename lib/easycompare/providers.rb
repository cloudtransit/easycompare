require 'aws-sdk'

module Provider

  class ProviderTemplate

    attr_reader :datacenter

    attr_reader :infrastructure

    # Creates object to interact with a provider.
    # @param json [Hash] provider specific settings
    #
    def initialize(json)

    end

    # Textual representation of the provider.
    # Used to generate understandable messages.
    # @return String of the form '<provider> (<datacenter>)', e.g. 'AWS (eu-central-1)'
    #
    def to_s
      "A Provider"
    end

    # List of burst instance types which show a processing
    # burst behaviour but have a general low processing performance.
    # Against these instances the processing benchmarking is only run
    # one time to avoid to loooong benchmark times.
    # The consequences of a reduced statistically relevance for these instance
    # types is hazarded.
    #
    # @return Array<String> List of instance types
    def burst_instances
      []
    end

    # Prepares provider infrastructure for benchmarking. This normally includes
    #
    # - maybe to create a virtual private network
    # - to create a security group
    #
    def prepare
    end

    # Deploys nodes to be benchmarked.
    #
    def deploy
    end

    # Returns DNS names of all nodes (without the network performance node).
    # @return Hash { ... }
    #
    def benchmark_nodes
    end

    # Returns the DNS name of the network performance node.
    # @return Hash { , ... }
    #
    def network_server_node
    end

    # Destroys all ressources created for benchmarking.
    #
    def clean
    end

  end

  # The {AWS} provider enables it to run benchmarks against
  # the Amazon Web Services EC2 Infrastructure.
  #
  class AWS < ProviderTemplate

    attr_reader :instance_type_iperf

    attr_reader :instances

    attr_reader :ec2

    def initialize(json)
      super(json)

      @infrastructure = 'AWS'
      @datacenter     = json['region']

      akid      = json['credentials']['access_key_identifier']
      secret    = json['credentials']['secret_access_key']
      @keyname  = json['keyname']
      @instance_type_iperf = json['iperf-type']
      @instances = json['instances']

      client = Aws::EC2::Client.new(
          access_key_id: akid,
          secret_access_key: secret,
          region: self.datacenter
      )

      @ec2 = Aws::EC2::Resource.new(client: client)
    end

    def to_s
      "#{@infrastructure} (#{@datacenter})"
    end

    # List of burst instance types which show a processing
    # burst behaviour but have a general low processing performance.
    # Against these instances the processing benchmarking is only run
    # one time to avoid to loooong benchmark times.
    # The consequences of a reduced statistically relevance for these instance
    # types is hazarded.
    #
    # @return Array<String> List of instance types
    def burst_instances
      ['t2.micro', 't2.small', 't2.medium']
    end


    #
    def prepare

      # create a security group using the default VPC or the EC2 classic variant
      # figure out a vpc named 'default' (we will take the first one we find under name 'default')
      default = [{ name: 'tag:name', values: ['default']}]
      defaults = self.ec2.vpcs(filters: default)
      sg = {
        group_name: 'easy-compare',
        description: 'Security Group to do an easy compare'
      }
      sg[:vpc_id] = defaults.first.vpc_id unless defaults.entries.empty?
      mesos = self.ec2.create_security_group(sg)
      mesos.create_tags(tags: [{ key: 'easy-role', value: 'benchmark-group' }])

      mesos.authorize_ingress(ip_protocol: 'tcp', from_port: 22, to_port: 22, cidr_ip: '0.0.0.0/0')
      mesos.authorize_ingress(ip_protocol: 'tcp', from_port: 1, to_port: 65535, cidr_ip: '10.0.0.0/16')
      mesos.authorize_ingress(source_security_group_name: 'easy-compare')
    end

    # returns benchmark security_group_id
    def security_group_id
      sg = [{ name: 'tag:easy-role', values: ['benchmark-group'] }]
      self.ec2.security_groups(filters: sg).map { |sg| sg.id }.first
    end

    def deploy

      begin
        is = self.ec2.create_instances(
            :image_id => self.instance_type_iperf['image'],
            :instance_type => self.instance_type_iperf['instance-type'],
            :disable_api_termination => false,
            :key_name => @keyname,
            :security_group_ids => [self.security_group_id],
            :max_count => 1,
            :min_count => 1
        )
        is.each do |instance|
          instance.create_tags(tags: [{ key: 'easy-role', value: 'benchmark-server' }])
        end
      rescue => e
        say "We got the following error deploying an instance of type '#{self.instance_type_iperf['instance-type']}': #{e}"
        say "You may want to run the clean command."
        Process.exit(1)
      end

      is = []
      self.instances.each do |instance|
        instance['instance-types'].each do |type|
          begin
            is << self.ec2.create_instances(
                :image_id => instance['image'],
                :instance_type => type,
                :disable_api_termination => false,
                :key_name => @keyname,
                :security_group_ids => [self.security_group_id],
                :max_count => 1,
                :min_count => 1
            )
          rescue => e
            say "We got the following error deploying an instance of type '#{type}': #{e}"
          end
        end
      end

      is.each do |instance|
        instance.create_tags(tags: [{ key: 'easy-role', value: 'benchmark-node' }])
      end

      nodes = ([{ name: 'tag:easy-role', values: ['benchmark-node', 'benchmark-server']}])
      pendings = self.ec2.instances(filters: nodes)
                         .reject { |i| i.state[:code] != 0 }
                         .map { |i| i.id }

      say "Waiting for nodes #{pendings} to come up"
      self.ec2.client.wait_until(:instance_running, instance_ids: pendings) unless pendings.empty?
    end

    def benchmark_nodes
      filter = [
          { name: 'tag:easy-role',  values: ['benchmark-node'] },
          { name: 'instance-state-name', values: ['running'] }
      ]
      self.ec2.instances(filters: filter)
              .map { |i| { dns: i.public_dns_name, ip: i.private_ip_address, instance_type: i.instance_type } }
    end

    def network_server_node
      filter = [
          { name: 'tag:easy-role',  values: ['benchmark-server'] },
          { name: 'instance-state-name', values: ['running'] }
      ]
      self.ec2.instances(filters: filter)
          .map { |i| { dns: i.public_dns_name, ip: i.private_ip_address, instance_type: i.instance_type } }
          .first
    end

    def clean
      # Terminate all nodes
      filter_nodes = [
          { name: 'tag:easy-role', values: ['benchmark-node', 'benchmark-server'] },
          { name: 'instance-state-name', values: ['running'] }
      ]
      clear_tags = [{ key: 'tag:benchmark-role', value: '' }]
      ids = self.ec2.instances(filters: filter_nodes)
                    .map do |instance|
                      instance.terminate
                      instance.create_tags(tags: clear_tags)
                      instance.id
                    end

      # Wait until all running nodes are terminated
      self.ec2.client.wait_until(:instance_terminated, instance_ids: ids) unless ids.empty?

      # Delete security groups
      filter_sgs = [{ name: 'tag:easy-role', values: ['benchmark-group']}]
      self.ec2.security_groups(filters: filter_sgs).each { |group| group.delete }

    end

  end

  class GCE < ProviderTemplate

    attr_reader :project_id

    def initialize(json)

      super(json)

      @infrastructure = 'GCE'
      @datacenter     = json['zone']
      @project_id     = json['project_id']

      @keyfile  = json['keyfile']
      @instance_type_iperf = json['iperf-type']
      @instances = json['instances']

      ret = `which gcloud`
      raise 'gcloud seems not to be installed. Please install \'gloud\': https://cloud.google.com/sdk/' if $? == "1"
    end

    def to_s
      "#{@infrastructure} (#{@datacenter})"
    end

    def gcloud
      "source ~/.bash_profile; gcloud compute --project #{@project_id}"
    end

    # List of burst instance types which show a processing
    # burst behaviour but have a general low processing performance.
    # Against these instances the processing benchmarking is only run
    # one time to avoid to loooong benchmark times.
    # The consequences of a reduced statistically relevance for these instance
    # types is hazarded.
    #
    # @return Array<String> List of instance types
    def burst_instances
      ['f1-micro', 'g1-small']
    end


    def benchmark_nodes
      json = `#{gcloud} instances list --zones #{@datacenter} --regexp easybench-node-.+ --format json`
      answer = JSON.parse(json)
      ret = answer.map do |node|
        say "#{node}"
        {
            dns: node['networkInterfaces'].first['accessConfigs'].first['natIP'],
            ip: node['networkInterfaces'].first['networkIP'],
            instance_type: node['machineType']
        }
      end
      say "#{ret}"
      ret
    end

    def network_server_node
      json = `#{gcloud} instances list --zones #{@datacenter} --regexp "easybench-iperf" --format json`
      raise 'Could not identify iperf server' unless $? == 0
      answer = JSON.parse(json)
      {
          dns: answer.first['networkInterfaces'].first['accessConfigs'].first['natIP'],
          ip: answer.first['networkInterfaces'].first['networkIP'],
          instance_type: answer.first['machineType']
      }
    end

    def prepare
    end

    def deploy
      iperf_image = @instance_type_iperf['image']
      iperf_type = @instance_type_iperf['machine-type']
      iperf_name = 'easybench-iperf'
      ret = `#{gcloud} instances create '#{iperf_name}' --zone #{@datacenter} --machine-type #{iperf_type} --image #{iperf_image}`
      if $? != 0
        say "Could not deploy #{iperf_name} (#{iperf_type} in zone #{@datacenter}) Error: #{ret}" if $? != 0
        say "You may want to run the clean command."
        Process.exit(1)
      end

      @instances.each do |machine_types|
        image = machine_types['image']
        machines = machine_types['machine-types']
        Parallel.each_with_index(machines, in_threads: machines.count) do |machine, i|
          name = "easybench-node-#{i + 1}"
          ret = `#{gcloud} instances create '#{name}' --zone #{@datacenter} --machine-type #{machine} --image #{image}`
          say "Could not deploy #{name} (#{machine} in zone #{@datacenter}) Error: #{ret}" if $? != 0
        end
      end

      # Adjust firewall settings
      public_ips = benchmark_nodes.map { |node| "#{node[:dns]}/32" } * ' '
      `#{gcloud} firewall-rules delete easybench-allow-public-ips --quiet`
      `#{gcloud} firewall-rules create easybench-allow-public-ips --allow tcp:1-65535 --source-ranges #{public_ips}`
    end

    def clean
      json = `#{gcloud} instances list --zones #{@datacenter} --regexp "easybench-.*" --format json`
      answer = JSON.parse(json)
      names = answer.map { |instance| instance['name'] } * ' '
      `#{gcloud} instances delete #{names} --zone #{@datacenter} --delete-disks all --quiet` unless names.empty?
      `#{gcloud} firewall-rules delete easybench-allow-public-ips --quiet`
    end

  end

end