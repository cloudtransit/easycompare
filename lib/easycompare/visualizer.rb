# Provides components to visualize benchmark data-
# The following submodules are provided.
#
# - {R} to generate visualizing scripts for the R framework (statistics framework)
# - {HTML} to generate HTML pages
# - {Tex} to generate Tex snippets for the Latex Typesetting system
# - {Console} to generate Console output
#
module Visualizer

  # Generates an instance name depending on provided level
  # if level == :datacenter then the datacenter is included into the instance name
  # otherwise not.
  def self.instance_name(v, level)
    center = ", #{v[:datacenter]}" if level == :datacenter
    "#{v[:type]}#{center}"
  end

  # Visualizer components to generate R.
  module R

    # Provides a right aligntment.
    def self.padright(a, n, pad)
      n <= a.count ? a : padright(a + [pad], n, pad)
    end

    # Generates a boxplot .
    # Generates a R script.
    def self.boxplot(data, bench, title, scale, level = :provider, labeled = false)
      max_n = data.map { |x| x[bench].count }.max
      names = "c(#{data.map { |x| "'#{ (labeled ? Visualizer::instance_name(x, level) : '') }'" } * ',' })"
      mar = "par(mar=c(10,5,1,1))"
      out = data.map do |x|
        "c(#{padright(x[bench], max_n, 'NA').map { |s| s.to_s } * ','})"
      end
      "boxplot(data.frame(#{out * ','}), main='#{title}', #{mar}, names=#{names}, las=2, ylab='#{scale}')\n"
    end

    # Visualizes benchmark data by boxplots.
    # Generates a R script.
    def self.visualize_data(data, level = :provider)

      vs = data #.map { |_, vector| vector }.sort { |v1, v2| v2[:threads] <=> v1[:threads] }

      out = "\n"
      out += "par(mfrow=c(3,2),cex.main=0.9,cex.lab=0.9,cex.axis=0.9,oma=c(1,1,1,1))\n"
      out += boxplot(vs, :processing, 'Processor performance (smaller is better)', 'Taychon, Time [s]', level, true)
      out += boxplot(vs, :memory, 'Memory transfer (higher is better)', 'Stream, Triad [MB/s]', level, true)
      out += boxplot(vs, :reads, 'Disk IO Read (higher is better)', 'IOZone, Read [MB/s]', level, true)
      out += boxplot(vs, :writes, 'Disk IO Write (higher is better)', 'IOZone, Write [MB/s]', level, true)
      out += boxplot(vs, :net, 'Network transfer (higher is better)', 'iperf [MB/s]', level, true)
      out
    end

  end

  # Visualizer components to generate HTML.
  module HTML

    def self.plot_scatter3d(data, title, xtitle, ytitle, ztitle, options = {})

      provider_series = data.values.group_by do |e|
        e[:iaas]
      end

      series = provider_series.map do |provider, data|
        points = data.map do |d|
          xsel = options[:xsel]
          ysel = options[:ysel]
          zsel = options[:zsel]

          {
              name: d[:type],
              x: d[xsel].respond_to?(:median) ? d[xsel].median : d[xsel],
              y: d[ysel].respond_to?(:median) ? d[ysel].median : d[ysel],
              z: d[zsel].respond_to?(:median) ? d[zsel].median : d[zsel]
          }
        end

        entries = points.map { |p| "{name: '#{p[:name]}', x: #{p[:x]}, y: #{p[:y]}, z: #{p[:z]} }" } * ','

        #print("<li>#{title}: #{entries}")

        """
         {
          name: '#{provider}',
          tooltip: {
            formatPoint: '{point.name}, {point.x}, {point.y}, {point.z}',
            snap: 0,
            animation: false,
            hideDelay: 10,
          },
          data: [#{entries}]
        }
        """
      end * ','

      id = "plot#{title.hash}"

      """
      <div class='scatterplot'>
      <div id='#{id}' class='plot'></div>
      <script>
      $(function() {
      var chart = new Highcharts.Chart({
         chart: {
            type: 'scatter',
            height: 600,
            renderTo: '#{id}',
            options3d: {
                beta: 0,
                alpha: 30,
                enabled: true,
                depth: 250,
                viewDistance: 5,

                frame: {
                    bottom: { size: 1, color: 'rgba(0,0,0,0.02)' },
                    back: { size: 1, color: 'rgba(0,0,0,0.04)' },
                    side: { size: 1, color: 'rgba(0,0,0,0.06)' }
                }
            }
        },
        legend: {
          enabled: true
        },
        title: {
            text: '#{title}'
        },
        xAxis: {
            title: {
                enabled: true,
                text: '#{xtitle}'
            }
        },
        yAxis: {
            title: {
                text: '#{ytitle}'
            }
        },
        zAxis: {
            title: {
                text: '#{ztitle}'
            },
        },
        plotOptions: {
            series: {
              stickyTracking: false,
              dataLabels: {
                enabled: true,
                format: '{point.name} ({point.series.name})',
              },
            },
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'black'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                dataLabels: {
                  padding: 0,
                  allowOverlap: false
                },
            }
        },
        series: [#{series}]
    });

    $(chart.container).bind('mousedown.hc touchstart.hc', function (e) {
        e = chart.pointer.normalize(e);

        var posX = e.pageX,
            posY = e.pageY,
            alpha = chart.options.chart.options3d.alpha,
            beta = chart.options.chart.options3d.beta,
            newAlpha,
            newBeta,
            sensitivity = 5;

            $(document).bind({
                'mousemove.hc touchdrag.hc': function (e) {

                    newBeta = beta + (posX - e.pageX) / sensitivity;
                    chart.options.chart.options3d.beta = newBeta;


                    newAlpha = alpha + (e.pageY - posY) / sensitivity;
                    chart.options.chart.options3d.alpha = newAlpha;

                    chart.redraw(false);
                },
                    'mouseup touchend': function () {
                    $(document).unbind('.hc');
                }
            });


    });

});

      </script>
      </div>
      """.gsub("\n", '')

    end

    def self.scatterplot(data, title, xtitle, ytitle, xsel = :processing, ysel = :memory, level = :provider)

      provider_series = data.values.group_by do |e|
        e[:iaas]
      end



      series = provider_series.map do |provider, data|
        points = data.map do |d|
          {
              name: d[:type],
              y: d[ysel].median,
              x: d[xsel].median
          }
        end

        entries = points.map { |p| "{name: '#{p[:name]}', x: #{p[:x]}, y: #{p[:y]} }" } * ','

        """
        {
          name: '#{provider}',
          data: [#{entries}]
        }
        """
      end * ","

      id = "plot#{title.hash}"

      """
      <div class='scatterplot'>
      <div id='#{id}' class='plot'></div>
      <script>
$(function () {
    $('##{id}').highcharts({
        chart: {
            type: 'scatter',
            zoomType: 'xy'
        },
        legend: {
          enabled: true
        },
        title: {
            text: '#{title}'
        },
        xAxis: {
            title: {
                enabled: true,
                text: '#{xtitle}'
            },
            type: 'logarithmic'
        },
        yAxis: {
            title: {
                text: '#{ytitle}'
            }
        },
        tooltip: {
           snap: 2,
           animation: false,
           crosshairs: [true, true],
           hideDelay: 10,
        },
        plotOptions: {
            series: {
              stickyTracking: false,
              dataLabels: {
                enabled: true,
                format: '{point.name} ({point.series.name})',
              }
            },
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'black'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<strong>VM type: </strong>',
                    pointFormat: '<strong>{point.name} ({point.series.name})</strong><br>#{xtitle}: {point.x}<br>#{ytitle}: {point.y}'
                }
            }
        },
        series: [#{series}]
    });
});

      </script>
      </div>
      """.gsub("\n", '')

    end

    def self.cell_highlight(similarity)
      out =  "background-color: rgba(64, 150, 238, #{similarity ** 3})"
      out += "; color: rgba(255,255,255,0.8)" if similarity > 0.9
      out += "; color: rgba(0,0,0,0.5)" if similarity <= 0.9
      out
    end

    def self.similarity_matrix(data1, data2, title)

      sims = data1.map do |i1|
        data2.map do |i2|
          {
              node1: i1[:type],
              node2: i2[:type],
              similarity: compare(i1, i2)
          }
        end
      end


      headers = ["<th><span>Similarities</span></th>"] + sims.first.map do |entry|
        "<th><span class='vertical'>#{entry[:node2]}</span></th>"
      end

      rows = sims.map do |line|
        """
        <tr>
          <td>#{ line.first[:node1] }</td>
          #{ line.map { |e| "<td style='#{cell_highlight e[:similarity]}'>#{'%.2f' % e[:similarity]}</td>" } * '' }
        </tr>
        """
      end

      """
      <div class='similarity'>
        <table>
          <caption>#{title}</caption>
          <tr>#{ headers * '' }</tr>
          #{ rows * '' }
        </table>
      </div>
      """.gsub("\n", '')
    end

    def self.boxplot(data, bench, title, scale, level = :provider, labeled = false, type: 'linear')

      provider_series = data.values.group_by { |machine_data| machine_data[:iaas] }

      plots = provider_series.map do |provider, machine_data|
        series = machine_data.map do |d|
          min = d[bench].min
          p25 = d[bench].percentile(25)
          med = d[bench].median
          p75 = d[bench].percentile(75)
          max = d[bench].max
          "[#{[min, p25, med, p75, max].map { |e| "#{e}" } * ','}]"
        end * ",\n"

        names = "[#{machine_data.map do |x|
          "'#{ (labeled ? Visualizer::instance_name(x, level) : '') }'"
        end * ',' }]"
        id = "#{provider}#{title.hash}"

        """
        <div class='boxplot'>
        <h3>#{provider} <small>#{title}</small></h3>
        <div id='plot#{id}' class='plot'></div>
        <script>
        $(function () {
          $('#plot#{id}').highcharts({

               credits: {
                  enabled: true,
                  text: 'nkode.io',
                  href: 'http://www.nkode.io'
               },

               exporting: {
                  enabled: false,
               },

               chart: {
                   type: 'boxplot',
               },

               title: {
                    text: ''
               },

               legend: {
                   enabled: false
               },

               xAxis: {
                   categories: #{names},
                   lineColor: '#000000',
                   labels: {
                      style: { color:'#000000', fontWeight:'bold' },
                    },
                    title: {
                       text: 'Virtual Machine Types'
                   }
               },

               yAxis: {
                   title: {
                       text: '#{scale}'
                   },
                   type: '#{type}'

               },

               plotOptions: {
                   boxplot: {
                       fillColor: '#EEEEEE',
                       color: '#000000',
                       lineWidth: 1,
                       medianColor: '#000000',
                       medianWidth: 3,
                       stemColor: '#000000',
                       stemDashStyle: 'line',
                       stemWidth: 1,
                       whiskerColor: '#000000',
                       whiskerLength: '50%',
                       whiskerWidth: 1
                   }
               },

               series: [{
                          name: '#{scale}',
                          animation: false,
                          data: [#{series}]
                        }]
           });
        });
        </script>
        </div>
        """.gsub("\n", '')
      end

      plots * ''

    end

    # Generates a DIV element
    def self.div(classes, content)
      element('div', content, classes)
    end

    def self.element(elem, content, classes= '')
      cs = classes == '' ? '' : " class='#{classes}'"
      "<#{elem}#{cs}>#{content}</#{elem}>"
    end

    # Generates a TD element
    def self.td(content, classes = '')
      element('td', content, classes)
    end

    # Generates a TH element
    def self.th(content, classes = '')
      element('th', content, classes)
    end

    def self.instances_table(data, title)
      provider_data = data.group_by { |entry| entry[1][:iaas] }

      tables = provider_data.map do |provider, data|

        rows = data.map do |instance, v|

          cores    = v[:cores]
          threads  = v[:threads]
          memory   = v[:memsize]

          disk_size = v[:disksize]
          disk_read = '%.02f' % v[:reads].median
          disk_read_n = v[:reads].count

          disk_write = '%.02f' % v[:writes].median
          disk_write_n = v[:writes].count

          mem = '%.02f' % v[:memory].median
          mem_n = v[:memory].count

          proc = '%.02f' % v[:processing].median
          proc_n = v[:processing].count

          netio = '%.02f' % v[:net].median
          netio_n = v[:net].count


          values = [td(instance)]
          values << td(cores)
          values << td(threads)
          values << td(memory)
          #values << td(disk_size)
          values << td("#{proc}<br><small>n=#{proc_n}</small>")
          values << td("#{mem}<br><small>n=#{mem_n}</small>")
          values << td("#{netio}<br><small>n=#{netio_n}</small>")
          values << td("#{disk_write}<br><small>n=#{disk_write_n}</small>")
          values << td("#{disk_read}<br><small>n=#{disk_read_n}</small>")

          "<tr>#{ values * '' }</tr>"
        end * ''

        table = "<table>"
        table += "<tr>" + ([
          th('Virtual Machine Type'),
          th('Cores', 'vertical'),
          th('Threads', 'vertical'),
          th('Memory<br><small>MB</small>', 'vertical'),
          th('Tachyon<br><small>s</small>', 'vertical'),
          th('Memory Transfer<br><small>MB/s</small>', 'vertical'),
          th('Network<br><small>MB/s</small>', 'vertical'),
          th('I/O Write<br><small>MB/s</small>', 'vertical'),
          th('I/O Read<br><small>MB/s</small>', 'vertical')
        ] * '') + "</tr>"
        table += "#{rows}"
        table += "<caption>#{title} of provider '#{provider}'</caption>"
        table += "</table>"

        "<div class='measured_data'>#{table}</div>"
      end

      tables * ''

    end

  end

  # Visualizer components to generate Tex.
  module Tex

    def self.tex_table(content, size: 'footnotesize', caption: '', rowspacing: '1pt')
      table =  "\\begin{table}\n"
      table += "\\#{size}\n"
      table += "\\setlength{\\tabcolsep}{#{rowspacing}}\n"
      table += content
      table += "\\caption{#{caption}}\n" unless caption == ''
      table += "\\end{table}\n"
      table
    end

    def self.instances_table(data, title)

      provider_data = data.values.group_by { |entry| entry[:iaas] }

      tables = provider_data.map do |provider, machines|
        tex_table = machines.map do |machine|

          instance = machine[:type]
          cores    = machine[:cores]
          threads  = machine[:threads]
          memory   = machine[:memsize]

          disk_size = machine[:disksize]
          disk_read = '%.02f' % machine[:reads].median
          disk_read_n = machine[:reads].count

          disk_write = '%.02f' % machine[:writes].median
          disk_write_n = machine[:writes].count

          mem = '%.02f' % machine[:memory].median
          mem_n = machine[:memory].count

          proc = machine[:processing].median
          proc_n = machine[:processing].count

          netio = '%.02f' % machine[:net].median
          netio_n = machine[:net].count

          [instance, cores, threads, memory, disk_size, proc, proc_n, mem, mem_n, netio, netio_n, disk_write, disk_write_n, disk_read, disk_read_n]

        end.map do |i|
          "#{i[0]} & #{i[2]} & #{i[3]} & #{'%.02f' % i[5]} \\newline \\tiny n=#{i[6]} & #{i[7]} \\newline \\tiny n=#{i[8]} & #{i[9]} \\newline \\tiny n=#{i[10]} & #{i[11]} \\newline \\tiny n=#{i[12]} & #{i[13]} \\newline \\tiny n=#{i[14]} \\\\"
        end * "\n"

        table  = "\\begin{tabular}{lrp{1.5cm}p{1.5cm}p{1.5cm}p{1.5cm}p{1.5cm}p{1.5cm}}\n"
        table += "\\textbf{VM Type}  & {\\textbf{Threads}} & \\textbf{Memory Size \\tiny(MB)} & \\textbf{Tachyon \\tiny(s)} & \\textbf{Memory Transfer\\tiny(MB/s)} & \\textbf{Network \\tiny(MB/s)} & \\textbf{I/O Write \\tiny(MB/s)} & \\textbf{I/O Read \\tiny(MB/s)} \\\\\n"
        table += "\\hline\n"
        table += "#{tex_table}\n"
        table += "\\end{tabular}\n"
        "#{ tex_table table, rowspacing: '4pt', caption: "#{title} of provider '#{provider}'"}"
      end

      tables * "\n\n"

    end

    def self.similarity_matrix(data1, data2, title)

      sims = data1.map do |v1|
        data2.map do |v2|
          {
              node1: v1[:type],
              node2: v2[:type],
              similarity: compare(v1, v2)
          }
        end
      end

      tab  = "\\begin{tabular}{l|#{'c' * sims.first.count }}\n"
      tab += "Similarities & #{sims.first.map { |e| "\\rotatebox{90}{#{e[:node2]}}" } * ' & '} \\\\\n"
      tab += "\\hline\n"
      tab += sims.map do |entries|
        "#{entries.first[:node1]} & #{ entries.map do |entry|
          color = entry[:similarity] > 0.9 ? "\\color{white}" : ''
          "\\cellcolor{black!#{(entry[:similarity] ** 3 * 100).to_i}} #{color} \\tiny #{'%.02f' % entry[:similarity]}"
        end * ' & '}\\\\"
      end * "\n"
      tab += "\n\\end{tabular}\n"

      "#{tex_table tab, size: 'scriptsize', caption: title }"

    end

  end

  module Console

    def self.instances_table(data, title)

      rows = data.map do |instance, v|

        cores    = v[:cores]
        threads  = v[:threads]
        memory   = v[:memsize]

        disk_size = v[:disksize]
        disk_read = v[:reads].median
        disk_read_n = "%5s" % "(#{v[:reads].count.to_s})"

        disk_write = v[:writes].median
        disk_write_n = "%5s" % "(#{v[:writes].count.to_s})"

        mem = v[:memory].median
        mem_n = "%5s" % "(#{v[:memory].count.to_s})"

        proc = v[:processing].median
        proc_n = "%5s" % "(#{v[:processing].count.to_s})"

        netio = v[:net].median
        netio_n = "%5s" % "(#{v[:net].count.to_s})"

        [instance, cores, threads, memory, disk_size, proc, proc_n, mem, mem_n, netio, netio_n, disk_write, disk_write_n, disk_read, disk_read_n]
      end.map do |i|
        [i[0], i[1], i[2], i[3], i[4], "#{'%.02f' % i[5]} #{i[6]}", "#{'%.02f' % i[7]} #{i[8]}", "#{'%.02f' % i[9]} #{i[10]}", "#{'%.02f' % i[11]} #{i[12]}", "#{'%.02f' % i[13]} #{i[14]}"]
      end

      table = Terminal::Table.new(
          headings: [
              'Instance Type',
              #              { value: 'Datacenter', alignment: :left },
              'Cores',
              'Threads',
              { value: "Memory\n[MB]", alignment: :right },
              { value: "Disk\n[GB]", alignment: :right },
              { value: "Tachyon\n[s] (n)", alignment: :right },
              { value: "Memory\n[MB/s] (n)", alignment: :right },
              { value: "Network\n[MB/s] (n)", alignment: :right },
              { value: "Disk Write\n[MB/s] (n)", alignment: :right },
              { value: "Disk Read\n[MB/s] (n)", alignment: :right }
          ],
          rows: rows
      )

      table.align_column(0, :left)
      table.align_column(1, :center)
      table.align_column(2, :right)
      table.align_column(3, :right)
      table.align_column(4, :right)
      table.align_column(5, :right)
      table.align_column(6, :right)
      table.align_column(7, :right)
      table.align_column(8, :right)
      table.align_column(9, :right)
      table.align_column(10, :right)
      table.align_column(11, :right)

      "#{title}\n#{table}"
    end
  end
end