require "easycompare/version"
require "easycompare/providers"
require "retriable"

module Easycompare
  # Your code goes here...

  def Easycompare::load(config)
    return Provider::AWS.new(config)       if config['provider'].downcase == 'aws'
    return Provider::GCE.new(config)       if config['provider'].downcase == 'gce'
    # return Provider::Azure.new(config)     if config['provider'].downcase == 'azure'
    # return Provider::OpenStack.new(config) if config['provider'].downcase == 'openstack'
    print "Sorry, Easycompare can not handle provider #{config['provider']}\n"
    Process.exit(1)
  end

  # Process results of a shell command.
  # This includes the
  #
  # - exit code of a process
  # - standard output of a process
  # - standard error of a process
  #
  # @author Nane Kratzke
  #
  class ProcessResult

    # Exit code of a process
    attr_reader :exit_code

    # Standard output of a process
    attr_reader :stdout

    # Standard error of a process
    attr_reader :stderr

    # Generates a ProcessResult object
    # @param code [Integer] exit code of a shell command
    # @param stdout [String] standard output of a shell command
    # @param stderr [String] standard error of a shell command
    def initialize(code, stdout, stderr)
      @exit_code = code
      @stdout = stdout
      @stderr = stderr
    end

    # String representation of the process result.
    # @return [String] process return in the following form: {:stdout=>String, :stderr=>String, :exit_code=>Integer}
    #
    def to_s
      {
          :exit_code => @exit_code,
          :stdout => @stdout,
          :stderr => @stderr
      }.to_s
    end
  end

  # Performs a command on a remote host.
  # @param ssh [SSH] ssh connection provided via Net::SSH.start() Block
  # @param command [String] a command to be executed on remote host
  # @return [ProcessResult] command return in the following form: {:stdout=>String, :stderr=>String, :exit_code=>Integer}
  #
  def Easycompare::ssh_exec!(ssh, command)
    stdout_data = ""
    stderr_data = ""
    exit_code = nil
    ssh.open_channel do |channel|
      channel.exec(command) do |ch, success|
        unless success
          abort "FAILED: couldn't execute command (ssh.channel.exec)"
        end
        channel.on_data do |ch,data|
          stdout_data+=data
        end

        channel.on_extended_data do |ch,type,data|
          stderr_data+=data
        end

        channel.on_request("exit-status") do |ch,data|
          exit_code = data.read_long
        end
      end
    end
    ssh.loop
    ProcessResult.new(exit_code, stdout_data, stderr_data)
  end

  # A {Node} is a host somewhere on the internet.
  # Each node has a public accessible DNS name.
  # Therefore, a node can be accessed via SSH by a
  # user and a users private key file.
  #
  # This class is used to instantiate objects capable to
  #
  # - connect to a node via SSH
  # - execute any commands on the node via an exec() method.
  #
  #   $ n = Node.new 'my.example.node', 'user', '/path/to/users/keyfile.pem'
  #   $ r = n.exec('ls - la')
  #   $ r.exit_code # => contains exit code of the command
  #   $ r.stdout    # => contains standard output of the command
  #   $ r.stderr    # => contains standard error of the command
  #
  # @author Nane Kratzke
  #
  class Node

    # @!attribute [r] host
    # @return [String] Public DNS name of the node
    attr_accessor :host

    # @!attribute [r] user
    #   @return [String] SSH user to admin the node
    attr_accessor :user

    # @!attribute [r] key
    # @return [String] Path to SSH keyfile used to admin the node
    attr_accessor :key

    # Instantiates a {Node} object being capable to interact with a host (virtual machine).
    #
    # @param host [String] DNS name of the node.
    # @param user [String] User name to login in to the node (this defaults to 'ubuntu')
    # @param key [String] Path to a private key file used to login as user *user* into node via SSH.
    # @param credential [String] cluster secret key (used to authenticate nodes and slaves)
    #
    def initialize(host, user, key)
      self.host = host
      self.user = user
      self.key  = key
    end

    # Writes content into a file on a node.
    #
    # @param dir [String] fully qualified directory name (e.g. '/this/is/an', this must be a valid UNIX directory descriptor string)
    # @param key [String] file name within dir (e.g. 'example', this must be a valid UNIX file name)
    # @param value [String] content to be written into the file (might not be nil but an empty string)
    # @return [Easycompare::ProcessResult] result of the operation
    #
    def set(dir, key, value)
      r = {}
      Retriable.retriable(intervals: [1, 5, 10, 15, 30, 60]) do
        r = self.exec([
          "mkdir -p #{dir}",
          "echo '#{value}' | tee #{dir}/#{key}"
        ])
      end
      r
    end

    # Executes a command on a node and returns its stdout.
    #
    # @param command [String] a valid bash command (e.g. 'ls -la')
    # @return [String] if exit code of command == 0 standard output of the command else nil
    #
    def do(command)
      r = {}
      Retriable.retriable(intervals: [1, 5, 10, 15, 30, 60]) do
        r = self.exec(["#{command}"])
      end
      r.exit_code == 0 ? r.stdout : nil
    end

    # Executes a command on a node.
    #
    # @param command [Array<String>] commands to be executed on node (e.g. ['ls -la', 'whoami'])
    # @return [Easymesos::ProcessResult] result of the operation
    #
    def exec(commands)
      r = ProcessResult.new(0, "", "")
      Retriable.retriable(intervals: [1, 5, 10, 15, 30, 60]) do
        Net::SSH.start(self.host, self.user, :keys => self.key, :paranoid => false) do |ssh|
          commands.each do |command|
            # say "Executing '#{command}' on host '#{self.host}'"
            r = Easycompare::ssh_exec!(ssh, command) if (r.exit_code == 0)
          end
        end
      end
      r
    end

  end

  class Benchmark

    # The tachyon benchmark is repeated 3 times by phoronix-test-suite.
    TACHYON_REP   = 10

    # The stream benchmark is repeated 2 times by phoronix-test-suite.
    STREAM_REP    = 5

    # The IOZone benchmark is repeated 7 times by phoronix-test-suite.
    IOZONE_REP    = 10

    # iperf benchmark runs are repeated 30 times.
    IPERF_REP     = 30

    # Each iperf benchmark run is done for 10 seconds.
    IPERF_DUR     = 10

    # Bash command to run the stream benchmark (triad) for memory benchmarking
    STREAM  = 'PRESET_OPTIONS="stream.run-type=3" phoronix-test-suite batch-run stream'

    # Bash command to run the tachyon benchmark for processor benchmarking
    TACHYON = 'phoronix-test-suite batch-run tachyon'

    # Bash command to run the IOZone benchmark (read/write of 512MB file with record-size of 1MB) for disk io benchmarking
    IOZONE  = 'PRESET_OPTIONS="iozone.record-size=2;iozone.file-size=0;iozone.test=2" phoronix-test-suite batch-run iozone'

    # Reference to the IaaS provider.
    attr_reader :provider

    # Reference to configuration data for the IaaS provider.
    attr_reader :config

    def initialize(config)

      @config   = config
      @provider = Easycompare::load(self.config)

    end

    def to_s
      "#{self.provider}"
    end

    # Prepares IaaS infrastructure telling the provider to
    # prepare its infrastructure for the benchmark and to
    # deploy the iperf server and the to be benchmarked instances.
    #
    def deploy
      print("Deploying to #{self.provider}\n")
      self.provider.prepare
      self.provider.deploy
      print("Deployed to #{self.provider}\n")
    end

    def phoronix_config
      File.open(File.dirname(__FILE__) + '/user-config.xml', 'rb').read
    end

    #
    def benchmarks_to_do
      nodes = self.provider.benchmark_nodes + [ self.provider.network_server_node ]
      nodes.count
    end

    # Installs benchmarking software on benchmark nodes and network server node.
    #
    def install
      nodes = self.provider.benchmark_nodes + [ self.provider.network_server_node ]

      print("Installing benchmarks on #{nodes.count} nodes\n")

      # Install benchmark nodes
      rs = Parallel.map(nodes, in_threads: nodes.count) do |node|
        n = Node.new node[:dns], self.config['user'], self.config['keyfile']

        begin
          r = n.exec([
             'sudo apt-get update',
             'sudo apt-get -y install iperf phoronix-test-suite build-essential',
             'yes | phoronix-test-suite batch-install pts/tachyon',
             'phoronix-test-suite batch-install pts/stream',
             'phoronix-test-suite batch-install pts/iozone'
          ])
          print("We got a remote execution problem on #{node[:dns]}. It is likely that the node is still booting. We will repeat the mission.\n") if (r.exit_code != 0)
        end while r.exit_code != 0

        r = n.set('~/.phoronix-test-suite', 'user-config.xml', self.phoronix_config) if r.exit_code == 0
        print("Benchmarks installed on node #{node[:dns]}\n")
        print("Benchmarks installed on node #{node[:dns]}\n")
        r
      end
    end

    # Run benchmarks.
    #
    def run
      # Starts the iperf server node as counter part for network benchmarking
      host = self.provider.network_server_node[:dns]
      user = self.config['user']
      key  = self.config['keyfile']

      Retriable.retriable(intervals: [1, 5, 10, 15, 30, 60]) do
        Net::SSH.start(host, user, :keys => key, :paranoid => false) { |ssh| ssh.exec('2>/dev/null 1>&2 iperf -s -D & exit') }
      end

      print("iperf server is running on #{host}\n")

      nodes = self.provider.benchmark_nodes

      print("Benchmarking #{nodes.count} nodes")

      results = Parallel.map(nodes, in_threads: nodes.count) do |node|
        n = Node.new node[:dns], self.config['user'], self.config['keyfile']
        r = nil

        r = n.exec(
          (1..STREAM_REP).map  { |i| STREAM } +
          (1..TACHYON_REP).map { |i| TACHYON } +
          (1..IOZONE_REP).map  { |i| IOZONE }
        )

        print("Node #{node[:dns]} finished\n")
        print("Node #{node[:dns]} finished (except iperf)\n")

        sleep(10) # Cool down

        r = n.exec(['cat .phoronix-test-suite/test-results/*/composite.xml'])

        print("Fetched benchmark data from #{node[:dns]}\n")

        {
            system_info: n.do('phoronix-test-suite detailed-system-info'),
            dns: node[:dns],
            type: node[:instance_type],
            phoronix_data: r.stdout,
        }
      end

      # It is important _not_ to parallelize this step, otherwise the network server
      # had to serve n concurrent iperf benchmarks (so we would benchmark the iperf server
      # and not the network transfer performance of the node)
      results.map do |node|
        n = Node.new node[:dns], self.config['user'], self.config['keyfile']
        server = self.provider.network_server_node[:ip]

        print("Running iperf benchmark on node #{node[:dns]}\n")

        iperf_out = ""
        (1..IPERF_REP).each do |i|
          r = n.exec(["iperf -c #{server} -f M -t #{IPERF_DUR}"])
          iperf_out << "#{ r.stdout }"
        end

        iperf_data = iperf_out.gsub(/\d+\.?\d* MBytes\/sec/).map { |s| s.gsub(/\d+\.?\d*/).first.to_f }

        print("Fetched iperf benchmark data from #{node[:dns]}\n")
        print("iperf benchmarking on node #{node[:dns]} completed\n")

        {
            'infrastructure' => self.provider.infrastructure,
            'datacenter' => self.provider.datacenter,
            'system_info' => node[:system_info],
            'dns' => node[:dns],
            'type' => node[:type],
            'phoronix' => node[:phoronix_data],
            'iperf' => iperf_data
        }
      end

    end

    # Destroys all nodes which have been deployed to perform the benchmark.
    #
    def clean
      print("Cleaning provider #{self.provider}\n")
      self.provider.clean
      print("Cleaned provider #{self.provider}\n")
    end

  end
end
