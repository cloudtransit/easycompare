require 'rubygems'
require 'commander'
require 'aws-sdk'
require 'net/ssh'
require 'parallel'
require 'json'
require 'descriptive_statistics'
require 'nokogiri'
require 'terminal-table'
require 'powerbar'
require 'rinruby'
require 'easycompare'
require 'easycompare/visualizer'
require 'easycompare/analyzer'

def analyze_system_info(system_info)
  if system_info != nil
    memory = system_info.gsub(/Memory: +\d+MB/).first.gsub(/\d+/).first.to_i
    cores = system_info.gsub(/Core Count: +\d+/).first.gsub(/\d+/).first.to_i
    threads = system_info.gsub(/Thread Count: +\d+/).first.gsub(/\d+/).first.to_i
    disksize = system_info.gsub(/Disk: +\d+GB/).first.gsub(/\d+/).first.to_i
  else
    memory = nil
    cores = nil
    threads =  nil
    disksize = nil
  end

  {
      cores: cores,
      threads: threads,
      memory: memory,
      disksize: disksize
  }
end

def stats(values)
  {
      median: values.median,
      n: values.number,
      sd: values.standard_deviation,
      samples: values,
      range: values.range,
      min: values.min,
      max: values.max,
      p5: values.percentile(5),
      p25: values.percentile(25),
      p75: values.percentile(75),
      p95: values.percentile(95),
  }
end

def get_median_for(description, xml, default = '?')
  median = analyze_phoronix_for(description, xml)[:median]
  median == nil ? default : format('%2.f', median)
end

def get_samples_for(description, xml, default = '?')
  median = analyze_phoronix_for(description, xml)[:samples]
  median == nil ? default : format('%2.f', median)
end

def analyze_phoronix_for(description, xml)
  xml_docs = xml.split('<?xml version="1.0"?>').reject { |s| s.empty? }
  values = []
  xml_docs.each do |xml|
    xml_doc = Nokogiri::XML('<?xml version="1.0"?>' + xml)
    results = xml_doc.xpath("//Result/Description[contains(text(), '#{description}')]").map { |n| n.parent }
    values << results.map { |result| result.xpath('Data/Entry/RawString').map { |rs| rs.content.split(':').map { |v| v.to_f } }.flatten }.flatten
  end
  stats values.flatten
end

def get_vector(data)

  type     = data['type']
  provider = data['infrastructure']
  datacenter = data['datacenter']
  info     = analyze_system_info(data['system_info'])
  cores    = info[:cores]
  threads  = info[:threads]
  memsize  = info[:memory]
  disksize = info[:disksize]

  disk_reads = analyze_phoronix_for('Record Size: 1MB - File Size: 512MB - Disk Test: Read Performance', data['phoronix'])[:samples]
  disk_writes = analyze_phoronix_for('Record Size: 1MB - File Size: 512MB - Disk Test: Write Performance', data['phoronix'])[:samples]
  mem_transfers = analyze_phoronix_for('Type: Triad', data['phoronix'])[:samples]
  proc_times = analyze_phoronix_for('Total Time', data['phoronix'])[:samples]
  net_transfers = data['iperf']
  measurement_times = Analyzer::Phoronix::times(data['phoronix'])

  {
      type: type,
      iaas: provider,
      datacenter: datacenter,
      cores: cores,
      threads: threads,
      memsize: memsize,
      processing: proc_times,
      memory: mem_transfers,
      disksize: disksize,
      reads: disk_reads,
      writes: disk_writes,
      net: net_transfers,
      times: measurement_times
  }

end

# Helper for Standardized Euclidian Distance Measure
def rel(v1, v2, symbol, median: true)
  x = median ? v1[symbol].median : v1[symbol]
  y = median ? v2[symbol].median : v2[symbol]
  [x, y].min / [x, y].max
end

# Standardized Euclidian Distance Measure
def euclidian v1, v2
  c  = (1 - rel(v1, v2, :cores, median: false)) ** 2
  t  = (1 - rel(v1, v2, :threads, median: false)) ** 2
  p  = (1 - rel(v1, v2, :processing)) ** 2
  ms = (1 - rel(v1, v2, :memsize, median: false)) ** 2
  mt = (1 - rel(v1, v2, :memory)) ** 2
  d  = (1 - rel(v1, v2, :disksize, median: false)) ** 2
  r  = (1 - rel(v1, v2, :reads)) ** 2
  w  = (1 - rel(v1, v2, :writes)) ** 2
  n  = (1 - rel(v1, v2, :net)) ** 2

  vs = [t, p, ms, mt, r, w, n]
  1 - vs.sum / vs.count
end

# Cosine Similarity Measure
def cosine v1, v2
  i = [
      v1[:threads],
      v1[:processing].median,
      v1[:memsize],
      v1[:memory].median,
      v1[:reads].median,
      v1[:writes].median,
      v1[:net].median
  ]

  j = [
      v2[:threads],
      v2[:processing].median,
      v2[:memsize],
      v2[:memory].median,
      v2[:reads].median,
      v2[:writes].median,
      v2[:net].median
  ]

  ijs = i.zip(j).map { |x| x[0] * x[1] }.sum
  is = Math.sqrt(i.map { |x| x ** 2 }.sum)
  js = Math.sqrt(j.map { |x| x ** 2 }.sum)

  ijs / (is * js)
end

# Dice Similarity Measure
def dice v1, v2

  i = [
      v1[:threads],
      v1[:processing].median,
      v1[:memsize],
      v1[:memory].median,
      v1[:reads].median,
      v1[:writes].median,
      v1[:net].median
  ]

  j = [
      v2[:threads],
      v2[:processing].median,
      v2[:memsize],
      v2[:memory].median,
      v2[:reads].median,
      v2[:writes].median,
      v2[:net].median
  ]

  ijs = i.zip(j).map { |x| x[0] * x[1] }.sum
  is = i.map { |x| x ** 2 }.sum
  js = j.map { |x| x ** 2 }.sum

  2 * ijs / (is + js)

end

# Compares two performance vectors by a similarity measure
def compare v1, v2
  euclidian(v1, v2)
end

def aggregate_data(data, level = :provider, type_filter: [], iaas_filter: [], datacenter_filter: [])
  instances = {}
  data.each do |d|
    v = get_vector(d)

    type     = v[:type]
    provider = v[:iaas]
    center   = level == :datacenter ? ", #{v[:datacenter]}" : ''

    key = "#{type} (#{provider}#{center})"

    if instances.key? key
      instances[key][:reads] += v[:reads]
      instances[key][:writes] += v[:writes]
      instances[key][:memory] += v[:memory]
      instances[key][:processing] += v[:processing]
      instances[key][:net] += v[:net]
      instances[key][:times] += v[:times]
      instances[key][:datacenters] += [v[:datacenter]]
      #instances[key][:datacenters].uniq!
    else
      instances[key] = v
      instances[key][:datacenters] = [v[:datacenter]]
    end
  end

  filter_data(instances, type_filter: type_filter, iaas_filter: iaas_filter, datacenter_filter: datacenter_filter)
end

def filter_data(data, type_filter: [], iaas_filter: [], datacenter_filter: [])
  ret = data
  ret = ret.select { |_, v| type_filter.include? v[:type].downcase } unless type_filter.empty?
  ret = ret.select { |_, v| iaas_filter.include? v[:iaas].downcase } unless iaas_filter.empty?
  ret = ret.select { |_, v| datacenter_filter.include? v[:datacenter].downcase } unless datacenter_filter.empty?
  ret.sort_by { |label, i| [-1 * i[:threads], -1 * i[:memsize], label] }.to_h
end

def load_data(files)
  files.map { |file| JSON.parse(File.open(file, 'rb').read) }
       .flatten
end

Commander.configure do

  program :version, Easycompare::VERSION
  program :description, 'Compares virtual machine type performances of IaaS cloud service providers.'
  program :name, 'easycompare'
  program :author, 'Nane Kratzke (nane.kratzke@fh-luebeck.de)'

  default_command :help

  command :visualize do |c|
    c.syntax = 'easycompare visualize [options] <data.json>+'
    c.summary = 'Generates an R script to visualize benchmark data of instance types as boxplot diagram.'
    c.option "--output STRING", String, "Output file"
    c.option '--datacenter', 'Analyze down to datacenter level (defaults to FALSE)'

    c.option '--types STRING', String, 'blabla'
    c.option '--iaas STRING', String, 'blabla'
    c.option '--datacenters STRING', String, 'blabla'

    c.option '--format STRING', String, "Output format (e.g. 'R', 'HTML')"

    c.action do |args, options|
      options.default :datacenter => FALSE, :types => '', :iaas => '', :datacenters => ''

      drilldown = options.datacenter ? :datacenter : :provider
      drilldown = :datacenter unless options.datacenters.empty?

      data = load_data(args)
      instances = aggregate_data(data, drilldown)

      is = filter_data(
           instances,
           type_filter: options.types.split(',').map { |s| s.strip.downcase },
           iaas_filter: options.iaas.split(',').map { |s| s.strip.downcase },
           datacenter_filter: options.datacenters.split(',').map { |s| s.strip.downcase }
      )

      #TODO WE HAVE TO CHECK/FIX R Generation and HTML Generation for multiple providers !!!

      out = Visualizer::R::visualize_data(is, options.datacenter ? :datacenter : :provider) if options.format == 'R'
      out = Visualizer::HTML::visualize_data(is, options.datacenter ? :datacenter : :provider) if options.format == 'HTML'
      File.write(options.output, out)
      say "#{out}"
    end
  end

  command :analyze do |c|
    c.syntax = 'easycompare analyze [options] <data.json>'
    c.summary = 'Analyzes benchmark data of instance types provided as a json file.'
    c.option '--datacenter', 'Analyze down to datacenter level (defaults to FALSE)'

    c.option '--types STRING', String, 'blabla'
    c.option '--iaas STRING', String, 'blabla'
    c.option '--datacenters STRING', String, 'blabla'

    c.option '--format STRING', String, "Output format (e.g. 'Tex', 'HTML', 'Console' [default])"
    c.option "--output STRING", String, "Output file"

    c.action do |args, options|
      options.default :datacenter => FALSE, :types => '', :iaas => '', :datacenters => ''
      options.default :format => 'console', :output => ''

      data = load_data(args)
      instances = aggregate_data(data, options.datacenter ? :datacenter : :provider)

      is = filter_data(
          instances,
          type_filter: options.types.split(',').map { |s| s.strip.downcase },
          iaas_filter: options.iaas.split(',').map { |s| s.strip.downcase },
          datacenter_filter: options.datacenters.split(',').map { |s| s.strip.downcase }
      )

      out = ""
      out = Visualizer::Tex::instances_table(is, 'Measured data') if options.format.downcase == 'tex'
      out = Visualizer::Console::instances_table(is, 'Measured data') if options.format.downcase == 'console'
      out = Visualizer::HTML::instances_table(is, 'Measured data') if options.format.downcase == 'html'


      File.write(options.output, out) unless options.output == ''
      say "#{out}"
    end
  end

  command :compare do |c|
    c.syntax = 'easycompare compare [options] <data.json>'
    c.summary = 'Compares two sets of virtual machine types.'
    c.option '--types STRING', String, 'Machine types to consider for first set.'
    c.option '--iaas STRING', String, 'Providers to consider for first set.'
    c.option '--datacenters STRING', String, 'Datacenters to consider for first set.'

    c.option '--with_types STRING', String, 'Machine types to consider for second set.'
    c.option '--with_iaas STRING', String, 'Providers to consider for second set.'
    c.option '--with_datacenters STRING', String, 'Datacenters to consider for second set.'

    c.option '--datacenter', 'blabla'

    c.option "--output STRING", String, "Output file"
    c.option '--format STRING', String, "Output format (e.g. 'Tex', 'HTML')"

    c.action do |args, options|

      options.default types: '', iaas: '', datacenters: ''
      options.default with_types: '', with_iaas: '', with_datacenters: ''
      options.default datacenter: false
      options.default format: 'HTML'
      options.default output: ''

      drilldown = options.datacenter ? :datacenter : :provider
      drilldown = :datacenter unless options.datacenters.empty?

      data = load_data(args)
      instances = aggregate_data(data, drilldown)

      v1s = filter_data(
          instances,
          type_filter: options.types.split(',').map { |s| s.strip.downcase },
          iaas_filter: options.iaas.split(',').map { |s| s.strip.downcase },
          datacenter_filter: options.datacenters.split(',').map { |s| s.strip.downcase }
      )

      v2s = filter_data(
          instances,
          type_filter: options.with_types.split(',').map { |s| s.strip.downcase },
          iaas_filter: options.with_iaas.split(',').map { |s| s.strip.downcase },
          datacenter_filter: options.with_datacenters.split(',').map { |s| s.strip.downcase }
      )

      title = 'Similarity values of various instance types (sorted on both axis by descending amount of simultaneous executable threads)'
      out = ""
      out = Visualizer::Tex::similarity_matrix(v1s, v2s, title) if options.format.downcase == 'tex'
      out = Visualizer::HTML::similarity_matrix(v1s, v2s, title) if options.format.downcase == 'html'

      File.write(options.output, out) unless options.output == ''
      say "#{out}"
    end
  end

  command :inspect do |c|
    c.syntax = 'easycompare inspect [options] <data-to-inspect.json>+'
    c.summary = 'Inspects collected benchmark-data. Useful to identify relevant filter criteria.'

    c.option '--types STRING', String, 'Machine types to consider for inspection.'
    c.option '--iaas STRING', String, 'Providers to consider for inspection.'
    c.option '--datacenters STRING', String, 'Datacenters to consider for inspection.'

    c.action do |args, options|

      options.default types: '', iaas: '', datacenters: ''

      data = load_data(args)
      instances = filter_data(
          aggregate_data(data, :datacenter),
          type_filter: options.types.split(',').map { |s| s.strip.downcase },
          iaas_filter: options.iaas.split(',').map { |s| s.strip.downcase },
          datacenter_filter: options.datacenters.split(',').map { |s| s.strip.downcase }
      )

      types = []
      datacenters = []
      iaas = []

      instances.each do |_, instance|
        iaas << instance[:iaas]
        datacenters << instance[:datacenter]
        types << instance[:type]
      end

      say "Types: #{types.uniq * ','}"
      say "Datacenters: #{datacenters.uniq * ','}"
      say "IaaS Providers: #{iaas.uniq * ','}"

    end
  end

  command :report do |c|

    c.syntax = 'easycompare report [options] <providers-to-compare.json>+'
    c.summary = 'Generates detailled reports of benchmark runs in several formats (e.g. HTML).'

    c.option '--types STRING', String, 'Machine types to consider for report.'
    c.option '--iaas STRING', String, 'Providers to consider for report.'
    c.option '--datacenters STRING', String, 'Datacenters to consider for report.'

    c.option '--format STRING', String, 'Output format. One of "html", "tex"'
    c.option '--texdir STRING', String, 'Output directory, e.g. "Desktop/report", defaults to "./report"'

    c.action do |args, options|
      require 'easycompare/reports'
      options.default types: '', iaas: '', datacenters: '', format: 'html', texdir: './report'

      data = load_data(args)
      instances = aggregate_data(data, options.datacenter ? :datacenter : :provider)

      is = filter_data(
          instances,
          type_filter: options.types.split(',').map { |s| s.strip.downcase },
          iaas_filter: options.iaas.split(',').map { |s| s.strip.downcase },
          datacenter_filter: options.datacenters.split(',').map { |s| s.strip.downcase }
      )

      print "#{Reports::Html::provider_report(is, 'IaaS Provider Performance Report')}\n" if options.format.downcase == 'html'
      File.write(
          "#{options.texdir}/report.tex",
          "#{Reports::Tex::provider_report(is, 'IaaS Provider Performance Report', options.texdir)}\n"
      ) if options.format.downcase == 'tex'

    end
  end

  command :similarity do |c|

    require 'pp'

    c.syntax = 'easycompare similarity [options] <providers-to-compare.json>+'
    c.summary = 'Generates a detailled similarity of different providers.'

    c.option '--types STRING', String, 'Comma-separated machine types to consider for report.'
    c.option '--iaas STRING', String, 'Comma separated providers to consider for report.'
    c.option '--datacenters STRING', String, 'Comma separated datacenters to consider for report.'

    c.action do |args, options|
      require 'easycompare/reports'
      options.default types: '', iaas: '', datacenters: ''
      data = load_data(args)
      instances = aggregate_data(data, options.datacenter ? :datacenter : :provider)

      is = filter_data(
          instances,
          type_filter: options.types.split(',').map { |s| s.strip.downcase },
          iaas_filter: options.iaas.split(',').map { |s| s.strip.downcase },
          datacenter_filter: options.datacenters.split(',').map { |s| s.strip.downcase }
      )

      print "#{Reports::Html::similarity_report(is, 'IaaS Similarity Report')}\n"

    end
  end

  command :collect do |c|

    c.syntax = 'easycompare collect [options] <providers-to-compare.json>+'
    c.summary = 'Runs benchmarks for specified machine types with different IaaS providers.'
    c.option '--output STRING', String, 'File to write the output'
    c.action do |args, options|

      options.default output: ''

      benchmarks = []

      if File.exist? options.output
        print "File #{options.output} already exist.\n"
        Process.exit(1)
      end

      begin
        benchmarks = JSON.parse(File.open(args[0], 'rb').read)
      rescue => e
        print "Could not open file: #{e}\n"
        Process.exit(1)
      end

      print("Starting benchmarking\n")

      outcome = Parallel.map(benchmarks) do |config|
        bm = Easycompare::Benchmark.new(config)
        begin
          print("Cleaning of #{bm} \n")
          bm.clean
          print("Deploying to #{bm} \n")
          bm.deploy
          print("Installing into #{bm} \n")
          bm.install
          print("Running benchmarks on #{bm} \n")
          results = bm.run
          print("Cleaning of #{bm} \n")
          bm.clean
          results
        rescue => e
          print "We got the following error in #{bm}: #{e}\n"
          nil
        end
      end.reject { |entry| entry == nil }.flatten

      begin
        print("Writing benchmark data\n")

        File.write(options.output, JSON.generate(outcome)) unless options.output == ''
        print "#{JSON.generate(outcome)}\n" if options.output == ''

        print("Benchmarking completed (see #{options.output}\n")
      rescue => e
        say "Could not store benchmark data due to #{e}"
      end
    end
  end

end