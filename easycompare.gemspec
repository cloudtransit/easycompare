# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'easycompare/version'

Gem::Specification.new do |spec|
  spec.name          = "easycompare"
  spec.version       = Easycompare::VERSION
  spec.authors       = ["Nane Kratzke"]
  spec.email         = ["nane.kratzke@googlemail.com"]
  spec.description   = %q{EasyCompare}
  spec.summary       = %q{EasyCompare}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "commander"
  spec.add_development_dependency "aws-sdk"
  spec.add_development_dependency "net-ssh"
  spec.add_development_dependency "parallel"
  spec.add_development_dependency "json"
  spec.add_development_dependency "retriable"
  spec.add_development_dependency "terminal-table"
  spec.add_development_dependency "descriptive_statistics"
  spec.add_development_dependency "nokogiri"
  spec.add_development_dependency "powerbar"
  spec.add_development_dependency "haml"
  spec.add_development_dependency "awesome_print"
  spec.add_development_dependency "rinruby"

end
